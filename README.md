# Description

My boss uses strange method to generate passwords. I have a dump of his desktop. Can you help me with extracting password?

# Writeup
Используя фреймворк volatility можно получить список процессов, увидеть что открыт openoffice calc и грепом по фразе mctf находим макрос. Цель - достать таблицу и макрос. Грепаем mctf - так получим код макроса. Гуглим, что такое ods файл, это xml-ка. Заканчивается </office:document-content>. Ищем, находим
<table:table-row table:style-name="ro1"><table:table-cell office:value-type="string"><text:p>9c43</text:p></table:table-cell><table:table-cell
office:value-type="string"><text:p>cc41</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>4223</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>c4de</text:p></table:table-cell
><table:table-cell office:value-type="string"><text:p>23e2</text:p></table:table-cell></table:table-row><table:table-row table:style-name="ro1"><table:table-cell office:value-type="string"><text:p>1234</text:p></table:table-cell><table:t
able-cell office:value-type="string"><text:p>ec58</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>e432</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>d221</text:p></table:t
able-cell><table:table-cell office:value-type="string"><text:p>5921</text:p></table:table-cell></table:table-row><table:table-row table:style-name="ro1"><table:table-cell office:value-type="string"><text:p>eec3</text:p></table:table-cell
><table:table-cell office:value-type="string"><text:p>98a1</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>7d17</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>e234</text:p>
</table:table-cell><table:table-cell office:value-type="string"><text:p>d518</text:p></table:table-cell></table:table-row><table:table-row table:style-name="ro1"><table:table-cell office:value-type="string"><text:p>ca7e</text:p></table:t
able-cell><table:table-cell office:value-type="string"><text:p>a212</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>7873</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>4892
</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>7919</text:p></table:table-cell></table:table-row><table:table-row table:style-name="ro1"><table:table-cell office:value-type="string"><text:p>1337</text:p>
</table:table-cell><table:table-cell office:value-type="float" office:value="8394"><text:p>8394</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>21e2</text:p></table:table-cell><table:table-cell office:valu
e-type="string"><text:p>2c2e</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>321e</text:p></table:table-cell></table:table-row><table:table-row table:style-name="ro1"><table:table-cell office:value-type="s
tring"><text:p>8086</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>2171</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>3901</text:p></table:table-cell><table:table-cell of
fice:value-type="string"><text:p>e583</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>4352</text:p></table:table-cell></table:table-row></table:table><table:table table:name="........2" table:style-name="t
a1" table:print="false"><table:table-column table:style-name="co1" table:default-cell-style-name="Default"/><table:table-row table:style-name="ro1"><table:table-cell/></table:table-row></table:table><table:table table:name="........3" ta
ble:style-name="ta1" table:print="false"><table:table-column table:style-name="co1" table:default-cell-style-name="Default"/><table:table-row table:style-name="ro1"><table:table-cell/></table:table-row></table:table></office:spreadsheet>
</office:body></office:document-content>
Это конец документа.
Ищем начало файла, можно по строке стилей. получаем
<style:font-face style:name="Mangal" svg:font-family="Mangal" style:font-family-generic="system" style:font-pitch="variable"/><style:font-face style:name="Microsoft YaHei" svg:font-family="&apos;Microsoft YaHei&apos;" style:font-family-g
eneric="system" style:font-pitch="variable"/><style:font-face style:name="Tahoma"
svg:font-family="Tahoma" style:font-family-generic="system" style:font-pitch="variable"/></office:font-face-decls><office:automatic-styles><style:style sty
le:name="co1" style:family="table-column"><style:table-column-properties fo:break-before="auto" style:column-width="2.267cm"/></style:style><style:style style:name="ro1" style:family="table-row"><style:table-row-properties style:row-heig
ht="0.453cm" fo:break-before="auto" style:use-optimal-row-height="true"/></style:style><style:style style:name="ro2" style:family="table-row"><style:table-row-properties style:row-height="0.473cm" fo:break-before="auto" style:use-optimal
-row-height="true"/></style:style><style:style style:name="ta1" style:family="table" style:master-page-name="Default"><style:table-properties table:display="true" style:writing-mode="lr-tb"/></style:style><number:text-style style:name="N
100"><number:text-content/></number:text-style><style:style style:name="ce1" style:family="table-cell" style:parent-style-name="Default" style:data-style-name="N100"/></office:automatic-styles><office:body><office:spreadsheet><table:tabl
e table:name="........1" table:style-name="ta1" table:print="false"><table:table-column table:style-name="co1" table:number-columns-repeated="5" table:default-cell-style-name="ce1"/><table:table-row table:style-name="ro1"><table:table-ce
ll office:value-type="string"><text:p>0836</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>323c</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>f4a1</text:p></table:table-ce
ll><table:table-cell office:value-type="string"><text:p>0327</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>9341</text:p></table:table-cell></table:table-row><table:table-row table:style-name="ro1"><table
:table-cell office:value-type="string"><text:p>1257</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>494c</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>33c7</text:p></table
:table-cell><table:table-cell office:value-type="string"><text:p>7007</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>72e1</text:p></table:table-cell></table:table-row><table:table-row table:style-name="ro
2"><table:table-cell office:value-type="string"><text:p>7346</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>9a3e</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>43e1</text:
p></table:table-cell><table:table-cell office:value-type="string"><text:p>beef</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>83d1</text:p></table:table-cell></table:table-row><table:table-row table:style
-name="ro1"><table:table-cell office:value-type="string"><text:p>9543</text:p></table:table-cell><table:table-cell office:value-type="float" office:value="4448"><text:p>4448</text:p></table:table-cell><table:table-cell office:value-type=
"string"><text:p>aab4</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>c102</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>9b41</text:p></table:table-cell></table:table-row>
<table:table-row table:style-name="ro2"><table:table-cell office:value-type="string"><text:p>2232</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>cead</text:p></table:table-cell><table:table-cell office:va
lue-type="string"><text:p>e7a2</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>66e3</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>0e3e</text:p></table:table-cell></table:

Дальше ищем между этими вещами. Находим кусок, в котором они "склеены" (например, ищем по 0e3e)
ell><table:table-cell office:value-type
="string"><text:p>9341</text:p></table:table-cell></table:table-row><table:table-row table:style-name="ro1"><table:table-cell office:value-type="string"><text:p>1257</text:p></table:table-cell><table:table-cell
office:value-type="string"
><text:p>494c</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>33c7</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>7007</text:p></table:table-cell><table:table-cell office:v
alue-type="string"><text:p>72e1</text:p></table:table-cell></table:table-row><table:table-row table:style-name="ro2"><table:table-cell office:value-type="string"><text:p>7346</text:p></table:table-cell><table:table-cell office:value-type
="string"><text:p>9a3e</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>43e1</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>beef</text:p></table:table-cell><table:table-cell
office:value-type="string"><text:p>83d1</text:p></table:table-cell></table:table-row><table:table-row table:style-name="ro1"><table:table-cell office:value-type="string"><text:p>9543</text:p></table:table-cell><table:table-cell office:v
alue-type="float" office:value="4448"><text:p>4448</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>aab4</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>c102</text:p></table:
table-cell><table:table-cell office:value-type="string"><text:p>9b41</text:p></table:table-cell></table:table-row><table:table-row table:style-name="ro2"><table:table-cell office:value-type="string"><text:p>2232</text:p></table:table-cel
l><table:table-cell office:value-type="string"><text:p>cead</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>e7a2</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>66e3</text:p
></table:table-cell><table:table-cell office:value-type="string"><text:p>0e3e</text:p></table:table-cell></table:table-row><table:table-row table:style-name="ro1"><table:table-cell office:value-type="string"><text:p>9c43</text:p></table:
table-cell><table:table-cell office:value-type="string"><text:p>cc41</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>4223</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>c4d
e</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>23e2</text:p></table:table-cell></table:table-row><table:table-row table:style-name="ro1"><table:table-cell office:value-type="string"><text:p>1234</text:p
></table:table-cell><table:table-cell office:value-type="string"><text:p>ec58</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>e432</text:p></table:table-cell><table:table-cell office:value-type="string"><t
ext:p>d221</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>5921</text:p></table:table-cell></table:table-row><table:table-row table:style-name="ro1"><table:table-cell office:value-type="string"><text:p>eec
3</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>98a1</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>7d17</text:p></table:table-cell><table:table-cell office:value-type="s
tring"><text:p>e234</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>d518</text:p></table:table-cell></table:table-row><table:table-row table:style-name="ro1"><table:table-cell office:value-type="string"><t
ext:p>ca7e</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>a212</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>7873</text:p></table:table-cell><table:table-cell office:valu
e-type="string"><text:p>4892</text:p></table:table-cell><table:table-cell office:value-type="string"><text:p>7919</text:p></table:table-cell></table:table-row><table:table-row table:style-name="ro1"><table:table-cell office:value-type="s
tring"><text:p>1337</text:p>
Мозаика сложилась, у нас есть табличка. Создаем ее, ищем макрос, находим. Складываем. Потом запустить макрос. 